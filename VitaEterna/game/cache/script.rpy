﻿# Coloca el código de tu juego en este archivo.


define gui.name_xpos = 950
define gui.name_ypos = -100
define gui.name_xalign = 0.5
define gui.name_xyalign = -1
define gui.namebox_width = 600
define gui.namebox_height = 100

transform lessright:
    xalign 0.85
    yalign 1.0
transform lessleft:
    xalign 0.15
    yalign 1.0
# define config.mouse = { "default" : [("gui/cursor.png",1,1)],}
#Fuente de los nombres            
style say_label:
    font "name_font.ttf"
    

# Declara los personajes usados en el juego como en el ejemplo:

# define e = Character("Eileen")
define p = Character("...")
define y = Character("Y2KR")
define m = Character("Mentor")
define narr = Character("Radio")
define alerta = Character("ALARMA")
define sonambiente = Character(" ")

default destruirobra = False
default renunciarinmortalidad = False
default sinrobots = False

# El juego comienza aquí.

label start:

    narr "¡Contemplen las maravillas de la ciencia!"

    narr "¡La inmortalidad al fin! Se acabaron del dolor y el sufrimiento. Para
    todos."

    narr "Los autorreparadores de VitaEterna(TM) le garantizarán la perfecta reconstrucción
    y sanación ante cualquier daño sufrido en minutos."

    narr "Vote por la iniciativa VitaEterna(TM) el próximo 2 de Febrero de 2817
    para que el gobierno garantice autorreparadores personales para toda la población"

    narr "El comienzo de una era sin sufrimiento está en su mano. Vote SÍ a la
    iniciativa VitaEterna(TM)."

    label dia1:
        scene bg sala_encendido prueba
        
        m "Levántate y anda..."

        m "Despierta, hijo mío. Si todo ha ido bien deberías poder oirme y
        entenderme."

        p "..."

        m "Espera."
        m "Prueba ahora. Debería haber conectado el circuito vocal antes de reactivarte." 
        show robot at lessleft:
            zoom 0.8
        p "...¿Qué está pasando?¿Dónde estoy?"
        show mentor at lessright:
            zoom 0.8

        m "La respuesta a tu segunda pregunta es que estamos en La sala de Arranque.
        La primera es mucho más complicada de responder."
        m "Simplificando mucho, espero que sea el comienzo de una nueva era."

        m "Parece que tus receptores visuales ya funcionan, ahora puedo responder a tus preguntas"

        menu:

            "¿Quién soy?":
                jump whoiam

            "¿Quién eres?":
                jump whoareyou

        label whoiam:

            m "Tu nombre es Y2K38 y eres uno de los robots autorreparadores que pueblan éste planeta"
            m "Y adelantándome a tu siguiente pregunta, soy uno de los diseñadores originales. He creado a miles de robots como tú"

            jump whatismymission

        label whoareyou:

            m "Soy uno de los diseñadores originales, he colaborado en la creación de miles de robots autorreparadores como tú"

            jump whatismymission

        label whatismymission:

            y "¿Robot autorreparador? Veo referencias a esa función en mi código, pero no encuentro comandos de ejecución relacionados
            con ellos."
            m "Eso es porque los he deshabilitado. Entorpecerían en tu función principal, la que he programado en tu interior y
            que te convierte en algo único."
            y "¿Cuál es esa función?"
            m "Destruir todo lo que ayudé a crear. Eliminar a los robots autorreparadores y liberar a la humanidad de la inmortalidad."

            menu:

                "No entiendo por qué alguien querría destruir su obra.":

                    jump destruir_obra

                "¿Por qué renunciaría la humanidad a algo así?":

                    jump renunciar_inmortalidad

        label destruir_obra:

            $ destruirobra = True
            m "..."
            m "Tendrías que haber visto lo que yo he visto."
            m "Ver tu obra pervertida y retorcida hasta el punto de poner en peligro el progreso humano."
            m "Me da ganas de vomitar. No puedo soportarlo, tiene que acabar."
            if renunciarinmortalidad:
                jump que_hacer
            else:
                jump renunciar_inmortalidad


        label renunciar_inmortalidad:

            $ renunciarinmortalidad = True
            m "La inmortalidad ha traído consigo consecuencias no deseadas. La humanidad ha dejado de tener miedo."
            m "Los seres humanos hemos estado aprendiendo de nuestros errores y creciendo y progresando gracias a superar
            nuestros miedos desde que salimos de las cavernas."
            y "Pero ahora no existe el miedo. No temen morir."
            m "Exacto. Tampoco hacerse daño. El dolor y el sufrimiento han sido completamente erradicados."
            if destruirobra:
                jump que_hacer
            else:
                p "¿Por eso has decidido destruir tu obra?"
                jump destruir_obra

        label que_hacer:

            y "¿Y qué tengo que hacer?"
            m "Paciencia. Mañana sabrás más. Ahora ha llegado el momento de desconectarte para que los sensores de la fábrica no puedan detectarte."
            y "¡Pero Padre! Tengo muchas preguntas."


    scene bg black
    label dia2:
        show bg black
        narr "Día 2"
        scene bg conducto
        show mentor  at lessright:
            zoom 0.8
        m "Despierta hijo mío."
        show robot at lessleft:
            zoom 0.8
        m "Ha llegado el momento de tu primera tarea."
        y "Padre. Necesito respuestas."
        m "De acuerdo, entiendo tu curiosidad. Pero comprende que trabajamos contrareloj. tenemos tiempo para una pregunta."

        menu:

            "¿Por qué debo ser yo quién haga esas tareas?":
                jump hacerlastareas

            "¿Qué es éste lugar? ¿Qué se hace en ésta fábrica?":
                jump lafabrica

            "¿Qué pasará si lo conseguimos?":
                jump ysiganamos

        label hacerlastareas:

            m "Hay un sector de la Fábrica a la que ningún humano puede acceder."
            m "Es un sistema de seguridad introducido para que los creadores no pudieramos alterar el programa básico de los autorreparadores."
            m "Con la programación que he insertado en tu interior, podrás acceder a él y ejecutar las tareas necesarias para la reprogramación."
            y "¿Sólo reprogramaremos a los autorreparadores? Eso es todo."
            m "..."
            m "Básicamente sí. Una reprogramación."
            y "¿Pero...?"
            jump solounapregunta1

        label lafabrica:

            #revisar y ampliar

            m "Aquí es donde se fabrican los autorreparadores y se les programa su función principal: Evitar cualquier tipo de daño a los humanos
            y reparar inmediatamente cualquier herida o enfermedad."
            m "Ésto es así para cualquier posible perjuicio."
            jump solounapregunta1


        label ysiganamos:

            m "Si ganamos se cerrará un ciclo que ha transcurrido durante demasiado tiempo."
            m "La humanidad podrá volver a avanzar. El dolor les hará aprender y progresar."
            m "Siglos de estancamiento tocarán a su fin y todo volverá a su curso."
            y "Y los humanos sufrirán."
            m "Nunca debimos dejar de hacerlo."
            m "Cómo afrontamos el sufrimiento es lo que nos define."
            y "Es un pensamiento extraño, Padre. Mis registros indican que evitar el sufrimiento ha sido siempre una de los principales objetivos
            de tu especie."
            y "Y ahora lo habéis conseguido. Habéis logrado vuestro mayor deseo."
            y "Pero tampoco os sirve."
            m "La insatisfacción es otro de nuestros rasgos más acusados. Pero aquí no se trata de eso, sino de que no sabíamos que lo que deseábamos
            destruiría lo que somos."
            y "Pero..."
            jump solounapregunta1

        label solounapregunta1:
            m "Basta. Te prometí una respuesta y la has obtenido. El tiempo juega en nuestra contra y tenemos que continuar."

            menu:
                "De acuerdo, Padre":
                    jump aceptar1

                "No es justo, tengo muchas preguntas":
                    jump rechazar1

            label aceptar1:
                    y "Dime qué tengo que hacer."
                    jump dia2continuacion

            label rechazar1:
                    y "Sólo tú tienes las respuestas que necesito."
                    m "Debes ser paciente. Todo llegará a su debido tiempo."
                    y "..."
                    y "¿Qué debo hacer, Padre?"
                    jump dia2continuacion

        label dia2continuacion:
                        
            scene bg conducto

            m "Hoy necesito que accedas a la sala de control y reprogrames los sistemas de seguridad con un código que te proporcionaré."
            y "¿Hoy? ¿No puedo realizar más que una tarea por día?"
            m "Es complicado de explicar, pero estás en lo cierto. Tendremos que realizar las tareas en días sucesivos."
            m "Además, es preciso que nadie detecte nuestra intrusión."
            m " Si algún otro creador detecta que hemos modificado la programación
            de los autorreparadores podrán revertir los cambios y destruir todo lo que llevo planeando tanto tiempo."
            m "Tendrás que hacer lo que sea preciso para evitarlo."
            y "De acuerdo Padre, muéstrame el camino"

            scene bg sala_control 

            m "Ahora está en tus manos. Sólo tienes que llegar al panel central y descargar el código que te he implantado."
            y "Pero ahí abajo hay otros robots como yo."
            m "Hay robots, en efecto, pero no como tú."

            # Falta descripción de las opciones

            menu:
                "Entrar en la sala":
                    y "Detectado"
                    jump dia3

                "Alterar voltaje de la parrilla":
                    y "Destrucción"
                    $ sinrobots = True
                    jump dia3

                "Lanzar algo al panel":
                    y "Desconexión"
                    $ sinrobots = True
                    jump dia3

    label dia3:
        scene bg conducto

        show robot at lessleft:
            zoom 0.8
        
        show mentor at lessright:
            zoom 0.8

        # Aquí debe haber un if para separar contenido según la opción elegida al final del día 2
        # si los robots te detectaron

        if sinrobots:
            # si destruiste a los robots
            m "Y2KR, es un nuevo día."
            m "Hoy podemos culminar nuestros trabajos."
            jump tareasdia3

        m "Despierta Y2KR."
        m "Tenemos trabajo por delante."
        jump tareasdia3        

        label tareasdia3:
            y "Padre, tengo más preguntas y creo que me he ganado algunas respuestas."
            m "Y2KR, ya te dije que tenemos muy poco tiempo."
            y "Lo se Padre, pero ésto no puede esperar."
            m "..."
            m "Muy bien, puedes formular una pregunta, y sólo una."

            # bloque if, la pregunta que se formuló en el día anterior ya no está
            menu:

                "¿Por qué debo ser yo quién haga esas tareas?":
                    jump hacerlastareas2
                
                "¿Qué es éste lugar? ¿Qué se hace en ésta fábrica?":
                    jump lafabrica2

                "¿Qué pasará si lo conseguimos?":
                    jump ysiganamos2

        label hacerlastareas2:

            m "Hay un sector de la Fábrica a la que ningún humano puede acceder."
            m "Es un sistema de seguridad introducido para que los creadores no pudieramos alterar el programa básico de los autorreparadores."
            m "Con la programación que he insertado en tu interior, podrás acceder a él y ejecutar las tareas necesarias para la reprogramación."
            y "¿Sólo reprogramaremos a los autorreparadores? Eso es todo."
            m "..."
            m "Básicamente sí. Una reprogramación."
            y "¿Pero...?"
            jump solounapregunta2

        label lafabrica2:

            #revisar y ampliar

            m "Aquí es donde se fabrican los autorreparadores y se les programa su función principal: Evitar cualquier tipo de daño a los humanos
            y reparar inmediatamente cualquier herida o enfermedad."
            m "Ésto es así para cualquier posible perjuicio."
            jump solounapregunta2


        label ysiganamos2:

            m "Si ganamos se cerrará un ciclo que ha transcurrido durante demasiado tiempo."
            m "La humanidad podrá volver a avanzar. El dolor les hará aprender y progresar."
            m "Siglos de estancamiento tocarán a su fin y todo volverá a su curso."
            y "Y los humanos sufrirán."
            m "Nunca debimos dejar de hacerlo."
            m "Cómo afrontamos el sufrimiento es lo que nos define."
            y "Es un pensamiento extraño, Padre. Mis registros indican que evitar el sufrimiento ha sido siempre una de los principales objetivos
            de tu especie."
            y "Y ahora lo habéis conseguido. Habéis logrado vuestro mayor deseo."
            y "Pero tampoco os sirve."
            m "La insatisfacción es otro de nuestros rasgos más acusados. Pero aquí no se trata de eso, sino de que no sabíamos que lo que deseábamos
            destruiría lo que somos."
            y "Pero..."
            jump solounapregunta2

        label solounapregunta2:
            m "Se acabó el tiempo Y2KR, tenemos que ponernos a trabajar."
            y "Pero padre, no se si lo que hice ayer fue lo correcto."
            m "¿Correcto?"
            m "Crees estás en situación de plantearte esos dilemas?"

            menu:
                "Si puedo hacer esa pregunta es por mi programación.":
                    jump miprogramacion

                "Sólo quiero saber si estoy haciendo bien mi trabajo.":
                    jump solohacermitrabajo

            label miprogramacion:
                m "..."
                m "Puede que tu programación básica te otorgue cierto libre albedrio, pero hay un momento para cada cosa."
                m "Y éste, definitivamente, no es el momento para esas cuestiones."
                y "Padre, ¿estás diciendome que debo hacer todo éste trabajo a ciegas?"
                m "Al menos hasta que hayamos triunfado."
                y "¿Cómo sabré que lo hemos hecho?"
                m "Porque yo te lo diré. Basta de preguntas, mi paciencia se ha agotado."
                jump tareasdia3continuacion

            label solohacermitrabajo:
                y "¿No seré más eficiente si conozco mejor los parámetros de mi trabajo?"
                m "No necesitas esa información para desempeñarlo."
                y "Pero..."
                m "Hijo. Se que tienes dudas, pero ahora mismo el tiempo escasea."
                m "Cada segundo que estás conectado nos arriesgamos a que nos detecten."
                y "De acuerdo padre, ¿Qué tengo que hacer?"
                jump tareasdia3continuacion

        label tareasdia3continuacion:

            m "La tarea de hoy es muy importante. Tenemos visita en la fábrica."
            y "Qué tipo de visita?"
            m "Diseñadores."
            y "¿Humanos?"
            m "Correcto."
            m "Cualquier detección en éste momento sería catastrófica."
            y "Tendré cuidado."

            scene bg sala_control 
            hide mentor prueba

            m "Frente a tí debe haber un panel."
            y "Sí."
            m "Encuentra los controles para los respiraderos de ventilación."
            y "Ya los he encontrado."
            m "Acciona el interruptor que está etiquetado cierre de seguridad."
            y "..."
            alerta "ADVERTENCIA: DETECTADA ONDAS CEREBRALES HUMANAS EN LA SALA. CONFIRMAR CIERRE."
            y "Padre, ¿qué estoy haciendo?"
            m "Lo que debe hacerse. ¡Confirma el cierre antes de que salte la alarma."

            if sinrobots:
                jump nohayrobotsparacurarles
            else:
                jump puedencurarles

            label nohayrobotsparacurarles:
                y "Padre, ¡Nadie podría curarles! No hay autorreparadores operativos en la fábrica."
                m "¡HAZLO!"
                y "¡Pero morirán!"
                m "Esa era la idea desde el principio. Deben morir para dar paso a la nueva era."
                y "No puedo hacerlo."
                m "¡Hazlo o te juro que te destruiré!"
                jump decisioncierreono

            label puedencurarles:
                y "¿Sólo quieres hacerles daño? Los autorreparadores les curarán."
                m "..."
                y "¿Padre?"
                m "Hazlo, todo saldrá bien. Estamos muy cerca de lograrlo."
                jump decisioncierreono

            label decisioncierreono:
                menu:
                    "Confirmar cierre":
                        jump cierreconfirmado
                    "Abortar cierre":
                        jump cierreabortado
                    "Alertar a los humanos":
                        jump alertaraloshumanos

            label cierreconfirmado:
                sonambiente "Las puertas de la sala de control se cierran y un gas denso llena la sala rápidamente."
                sonambiente "Gritos espeluznantes que son seguidos de sonidos ahogados y golpes."
                sonambiente "Finalmente. Silencio."
                m "¡Lo has conseguido! Ahora todo podrá avanzar a la siguiente fase."
                y "Padre ¿He hecho lo correcto?"
                m "Vuelve Y2KR. Lo has hecho muy bien hijo mío."
                hide robot prueba
                jump climaxvictoria

            label cierreabortado:
                alerta "CIERRE DE SEGURIDAD ABORTADO"
                m "¡¡NO!!"
                sonambiente "Gritos desde la sala de control indican que los humanos han descubierto lo que está ocurriendo."
                m "No, no, no. Estás arruinando todo por lo que he trabajado."
                y "Padre. No podía hacer algo así."
                m "No me llames Padre. Ésto no acabará así."
                hide robot prueba
                jump climaxabortado

            label alertaraloshumanos: #esta es la opcion que sólo debe aparecer si somos suficientemente rebeldes.
                y "¡Vosotros! ¡Huid! ¡Mi padre quiere haceros daño o algo peor!"
                m "¡¡NO!! QUÉ ESTÁS HACIENDO."
                y "Corred antes de que encuentre otra manera de dañaros."
                m "¡Estás destruyendo años de trabajo, insensato. No debí confiar en tí."
                m "Ésto no acabará así."
                hide robot prueba
                jump climaxhumanos


    scene bg conducto
    label climaxvictoria: 
        show bg conducto
    show robot prueba at lessleft:
        zoom 0.8
    show mentor prueba at lessright:
        zoom 0.8
    m "LO CONSEGUIMOS."
    y "..."
    m "Alégrate, hijo mío, has realizado tu trabajo maravillosamente."
    m "Sólo resta un paso: Destruir ésta fábrica y todo lo que contiene."
    y "No quiero hacerlo Padre. Ya he hecho demasiado."
    m "No te preocupes. Ahora los sistemas de seguridad están desactivados."
    m "Entraré en la sala de control y apuñalaré el corazón de ésta aberración con mis propias manos."
    show bg salacontrol prueba
    sonambiente "Los cadáveres de los humanos están aún esparcidos por el suelo, por suerte, no puedes ver sus caras."
    y "Siento deseos de repararlos, pero no encuentro esas funciones en mi programación."
    m "Fuiste creado así para que el plan pudiera funcionar."
    m "Cuando enviemos la señal de autodestrucción no quedarán robots autorreparadores ni nadie que pueda reconstruirlos."
    m "Poco a poco los humanos volverán a su ritmo natural."
    m "Hemos llegado. Cuando active la actualización, el código que incorporaste iniciará la autodestrucción."
    menu:
        "Impedirlo":
            jump atacarpadre
        "No hacer nada":
            jump permitirautodestruccion

    label atacarpadre:
        sonambiente "Antes de que pueda apretar el botón. Y2KR golpea a su padre con todas sus fuerzas."
        m "¿Qué crees que estás haciendo?"
        y "¡No puedo permitir que lo hagas!"
        m "Insensato... ya es demasiado tarde. Aunque no destruyamos a los robots, éstos acabarán desapareciendo."
        m "El ciclo se ha roto, sólo condenarás a la humanidad a un declive más lento."
        y "Encontraré la forma."
        sonambiente "Con un fuerte golpe, Y2KR rompe el cráneo de su padre. Después, silencio."
        jump fin

        label permitirautodestruccion:
            sonambiente "Al oprimir el botón, todas las pantallas se iluminan mientras se transmite el código."
            sonambiente "El número de robots autorreparadores activos va descendiendo, hasta el cero."
            alerta "SECUENCIA DE AUTODESTRUCCIÓN ACTIVADA"
            m "Y ha llegado el fin. Cuando la fábrica vuele por los aires todo habrá terminado."
            y "Pero nosotros también seremos destruidos."
            m "Así debe ser, hijo mío. Un nuevo mundo muere para que otro nazca."
            m "Lo has hecho muy bien."
            jump fin



    scene bg conducto
    label climaxabortado:
        show bg conducto
    show robot prueba at lessleft:
        zoom 0.8
    show mentor prueba at lessright:
        zoom 0.8
    m "¿Por qué lo has hecho?"
    y "Padre..."
    m "No me llames así. ¡Te he hecho una pregunta!"
    y "Hubiera matado a esa gente. No soy un asesino."
    m "Tu misión. Tu única función era completar mi trabajo."
    y "¿Me creaste sólo para asesinar a esas personas?"
    m "¡Te creé para acabar con ésta locura! ¿Qué son unas pocas vidas a cambio de retomar el control de nuestro destino."
    m "Ahora están sobre aviso. Me estarán buscando. Pero puedo volver a empezar de nuevo."
    m "Pero será sin tí."
    sonambiente "Y2KR no vio el arma que sostenía su padre hasta que fue demasiado tarde."
    jump fin

    scene bg conducto
    label climaxhumanos:
        show bg conducto
    show robot prueba at lessleft:
        zoom 0.8
    show mentor prueba at lessright:
        zoom 0.8
    
    m "Idiota. Todos nos están buscando. Nos encontrarán en minutos. No hay escapatoria."
    y "No podía permitir que les mataras."
    m "Te creé para hacerlo. Era tu única función. Has fracasado."
    y "Padre..."
    m "No me llames así. No tienes derecho. Voy a morir y mi obra se quedará incompleta."
    m "Has condenado a la humanidad a continuar en éste ciclo absurdo."
    sonambiente "Y2KR descubrió que su padre sostenía un arma."
    menu:
        "Atacarle":
            jump atacarleantes

        "Aceptar tu destino":
            jump destino

    label atacarleantes:
        sonambiente "Y2KR golpea a su padre con todas sus fuerzas antes de que pueda disparar."
        m "Es... el... fin..."
        y "No podía permitírtelo Padre."
        m "Idiota... les ... has condenado."
        y "Elegirán su propio destino."
        jump fin

    label destino:
        y "Sólo he querido darles la oportunidad de darse cuenta de su error."
        m "Idiota, si no han aprendido en éste tiempo, no lo harán nunca."
        y "Entonces deberías acabar conmigo."
        m "Podríamos haber creado un nuevo mundo, pero si voy a morir, vendrás conmigo."
        sonambiente "El disparo resonó en los túneles, junto con las pisadas de la gente que se aproximaba."
        jump fin

    label fin:
        sonambiente "FIN"
        return
